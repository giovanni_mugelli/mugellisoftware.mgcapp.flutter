import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MasterDetail extends StatefulWidget {
  final Widget child;

  // MasterDetail({Key key, this.child}) : super(key: key);
  MasterDetail({this.child});

  @override
  _MasterDetailState createState() => _MasterDetailState();
}

class _MasterDetailState extends State<MasterDetail>
    with SingleTickerProviderStateMixin {
  // set variables
  bool isCollapsed = true;
  double screenWidth, screenHeight;
  final Duration duration = const Duration(milliseconds: 200);
  AnimationController _controller;
  Animation<double> _scaleAnimation;
  Animation<double> _menuScaleAnimation;
  Animation<Offset> _slideAnimation;
  BorderRadius _borderRadius;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: duration);
    _scaleAnimation = Tween<double>(begin: 1, end: 0.8).animate(_controller);
    _menuScaleAnimation =
        Tween<double>(begin: 0.5, end: 1).animate(_controller);
    _slideAnimation = Tween<Offset>(begin: Offset(-1, 0), end: Offset(0, 0))
        .animate(_controller);
    _borderRadius = BorderRadius.all(Radius.circular(0));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Ottieni dimenzioni pagina
    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;

    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            menu(context),
            dashboard(context),
          ],
        ));
  }

  Widget menu(context) {
    return SlideTransition(
      position: _slideAnimation,
      child: ScaleTransition(
        scale: _menuScaleAnimation,
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("News",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontFamily: "Calibre-Semibold",
                    )),
                SizedBox(height: 20),
                Text("Calendario",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontFamily: "Calibre-Semibold",
                    )),
                SizedBox(height: 20),
                Text("Avvisi",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontFamily: "Calibre-Semibold",
                    )),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget dashboard(context) {
    return AnimatedPositioned(
      duration: duration,
      top: 0,
      bottom: 0,
      left: isCollapsed ? 0 : 0.5 * screenWidth,
      right: isCollapsed ? 0 : -0.2 * screenWidth,
      child: ScaleTransition(
          scale: _scaleAnimation,
          child: GestureDetector(
            onHorizontalDragDown: _onDragHorizontalDownMenu,
            onHorizontalDragStart: _onDragHorizontalStartMenu,
            child: Material(
                animationDuration: duration,
                borderRadius: _borderRadius,
                elevation: 10,
                color: Colors.white,
                child: SingleChildScrollView(
                    child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 12.0, right: 12.0, top: 40.0, bottom: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          IconButton(
                            icon: Icon(
                              FontAwesomeIcons.bars,
                              color: Colors.black,
                              size: 30.0,
                            ),
                            onPressed: _onPressedMenu,
                          )
                        ],
                      ),
                    ),
                    widget.child
                  ],
                ))),
          )),
    );
  }

  void _setStateCollasped() {
    setState(() {
      if (isCollapsed) {
        _controller.forward();
        _borderRadius = BorderRadius.all(Radius.circular(40));
      } else {
        _controller.reverse();
        _borderRadius = BorderRadius.all(Radius.circular(0));
      }

      isCollapsed = !isCollapsed;
    });
  }

  void _onPressedMenu() {
    if(!isCollapsed) {
      return;
    }

    _setStateCollasped();
  }

  void _onDragHorizontalDownMenu(DragDownDetails event) {
    if (!isCollapsed) {
      return;
    }

    _setStateCollasped();
  }

  void _onDragHorizontalStartMenu(DragStartDetails details) {
    if (isCollapsed && details.globalPosition.dx > 50) {
      return;
    }

    _setStateCollasped();
  }
}
