import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mgc/master_detail.dart';
import 'home.dart';

void main() => runApp(MaterialApp(home: MyApp()));
// void main() => runApp(MaterialApp(home: MenuDashboardPage()));

final Color backgroundColor = Color(0xFFFFFFFF);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MasterDetail(child: Home());
  }
}
