import 'package:flutter/cupertino.dart';

class TabBottom {
  // TabBottom({this.items});

  // final Tab items;

  static CupertinoTabBar build() {
    return CupertinoTabBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(icon: Icon(CupertinoIcons.car)),
        BottomNavigationBarItem(icon: Icon(CupertinoIcons.gear)),
      ],
    );
  }
}
