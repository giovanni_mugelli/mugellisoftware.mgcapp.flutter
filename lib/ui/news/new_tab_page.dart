import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mgc/ui/news/new_ui.dart';

class NewTabPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('News'),
      ),
      // child: Center(s),
      child: ListView(children: <Widget>[
        NewUi(),
        NewUi(),
        NewUi(),
      ]),
    );
  }
}
