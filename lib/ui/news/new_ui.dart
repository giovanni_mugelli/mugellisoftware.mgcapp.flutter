import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewUi extends StatelessWidget {
  // NewUi({});

  static const BorderRadius borderRadius = BorderRadius.all(Radius.circular(10));

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(15.0),
        child: DecoratedBox(
          decoration: BoxDecoration(
              color: Color(0xffffffff),
              borderRadius: borderRadius,
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10,
                  offset: Offset(0, 10),
                )
              ]),
          child: ClipRRect(
              borderRadius: borderRadius,
              child: Image.asset('assets/images/img1.jpg',height: 500, fit: BoxFit.cover,)),
        ));

    // return Image.asset('assets/images/img1.jpg', width: 100.0, height: 100.0);
  }
}
