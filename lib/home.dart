import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'badge.dart';
import 'cardScrollWidget.dart';
import 'images.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var currentPage = images.length - 1.0;

  @override
  Widget build(BuildContext context) {
    // set controller e current page
    var controller = PageController(initialPage: images.length - 1);
    controller.addListener(() {
      setState(() {
        currentPage = controller.page;
      });
    });

    // return SingleChildScrollView(
    // scrollDirection: Axis.vertical,
    // physics: ClampingScrollPhysics(),
    // add layout to viewport contraint
    //  constraints: BoxConstraints(
    //     minHeight: viewportConstraints.maxHeight,
    //   )
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Notizie",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 40.0,
                      letterSpacing: 1.0,
                      fontFamily: "Calibre-Semibold")),
              IconButton(
                icon: Icon(
                  FontAwesomeIcons.ellipsisH,
                  size: 30.0,
                  color: Colors.black,
                ),
                onPressed: () {},
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Row(
            children: <Widget>[
              Badge(title: "News", color: Color(0xFFff6e6e)),
              SizedBox(width: 15.0),
              Text("10+ Articoli", style: TextStyle(color: Colors.blueAccent))
            ],
          ),
        ),
        Stack(
          children: <Widget>[
            CardScrollWidget(currentPage),
            Positioned.fill(
              child: PageView.builder(
                itemCount: images.length,
                controller: controller,
                reverse: true,
                itemBuilder: (context, index) {
                  return Container();
                },
              ),
            )
          ],
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 6.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Vecchi",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 46.0,
                      fontFamily: "Calibre-Semibold",
                      letterSpacing: 1.0)),
              IconButton(
                icon: Icon(
                  FontAwesomeIcons.ellipsisH,
                  size: 12.0,
                  color: Colors.white,
                ),
                onPressed: () {},
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Row(
            children: <Widget>[
              Badge(title: "Passati", color: Colors.blueAccent),
              SizedBox(width: 15.0),
              Text("450k+ Articoli", style: TextStyle(color: Colors.blueAccent))
            ],
          ),
        ),
        SizedBox(height: 20.0),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                  // decoration: BoxDecoration(
                  //     borderRadius: BorderRadius.all(Radius.circular(0))),
                  padding: EdgeInsets.only(
                      left: 20.0, bottom: 20, right: 20),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(16)),
                      child: Image.asset('assets/images/img1.jpg',
                          fit: BoxFit.cover))),
            )
          ],
        ),
        // SizedBox(height: 20.0),
        // Row(
        //   children: <Widget>[
        //     Padding(
        //       padding: EdgeInsets.only(left: 20.0, bottom: 18),
        //       child: ClipRRect(
        //         borderRadius: BorderRadius.circular(20.0),
        //         child: Image.asset("assets/images/img1.jpg",
        //             width: 296.0, height: 222.0),
        //       ),
        //     )
        //   ],
        // )
      ],
    );
    // );
  }
}
