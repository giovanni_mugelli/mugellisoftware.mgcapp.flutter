import 'package:flutter/material.dart';

class Badge extends StatelessWidget {
  final String title;
  final Color color;

  Badge({this.title, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 22.0, vertical: 6.0),
          child: Text(title, style: TextStyle(color: Colors.white)),
        ),
      ),
    );
  }
}
